-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2013 at 03:43 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cmse`
--

-- --------------------------------------------------------

--
-- Table structure for table `cmse_pages`
--

CREATE TABLE IF NOT EXISTS `cmse_pages` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) NOT NULL,
  `Content` longtext NOT NULL,
  `URL` varchar(50) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cmse_pages`
--

INSERT INTO `cmse_pages` (`ID`, `Title`, `Content`, `URL`, `UserID`, `Date`) VALUES
(1, 'Header', 'fsdfsdfsdf', '<p>sfsdfsdfsdfsdf</p>', 4, '2013-07-20 13:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `cmse_userauth`
--

CREATE TABLE IF NOT EXISTS `cmse_userauth` (
  `UserID` int(11) NOT NULL,
  `AuthKey` binary(16) NOT NULL,
  `Date` datetime NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`UserID`,`AuthKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cmse_userauth`
--

INSERT INTO `cmse_userauth` (`UserID`, `AuthKey`, `Date`, `Active`) VALUES
(4, 'S+�@df.�X�U��[�', '2013-07-20 12:29:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cmse_users`
--

CREATE TABLE IF NOT EXISTS `cmse_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `RegDate` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cmse_users`
--

INSERT INTO `cmse_users` (`ID`, `Username`, `Password`, `Email`, `RegDate`) VALUES
(2, 'Pkshields2', '$P$B0pQLDWT5xfZ/QLHI7HcY5iZcrTMYA1', 'pkshields@gmail.com', '0000-00-00 00:00:00'),
(3, 'Pkshields3', '$P$B/srIG7twB6HxeomrpdoUPTCBXLg6U0', 'pkshields@gmail.com', '2013-07-13 15:24:12'),
(4, 'Pkshields', '$P$BWddMljxpGex24pZPSuhVdzruoI4tM1', 'pkshields@gmail.com', '2013-07-13 22:11:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
