<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="<?PHP echo $themeURL; ?>favicon.ico" type="image/x-icon" />
	<title>CMSE Default Theme</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, cme" /> 
	<meta name="Description" content="CME Exchange!" />

	<link rel="stylesheet" type="text/css" href="<?PHP echo $themeURL; ?>style.default.css" />
</head>
<body>
	<div id="container">
		<div id="header">
			<h1>CMSE Default Theme</h1>
		</div>
		<div id="links">
			<p><a href="#">Links</a> | <a href="#">One</a> | <a href="#">Two</a> | <a href="#">Muzene</a> | <a href="#">Pkshields</a> | <a href="#">CME</a></p>
		</div>
		<div id="content">
			<h2><?PHP echo $title; ?></h2>
			<p><?PHP echo $content; ?></p>
		</div>
		<div id="footer">
			<p>Footer</p>
		</div>
	</div>
</body>
</html>