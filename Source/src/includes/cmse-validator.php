<?PHP 
/**
 * This file holds the CMSE Validation class, used to validate various types on input
 *
 * @author Paul Shields - Pkshields.com
 */

	/**
	 * CMSE Validation
	 */
	class CMSEValidator
	{
		/**
		 * Check that the URL entered contains valid characters we support
		 *
		 * @param int url	URL we are checking
		 *
		 * @return bool 	Result of check
		 */
		public static function ValidateURL($url)
		{
			//Allow alphanumeric, /, \ - and _ only.
			$regex = '/^[a-zA-Z0-9\/\-_]*$/';
			return preg_match($regex, $url);
		}

		/**
		 * Check that the vallue entered is an integer (in string form)
		 *
		 * @param int value		Value we are checking
		 *
		 * @return bool 		Result of check
		 */
		public static function ValidateNum($value)
		{
			//Allow numbers only in string
			$regex = '/^[0-9]*$/';
			return preg_match($regex, $value);
		}

		/**
		 * Check that the value entered is alphanumberic
		 *
		 * @param int value		Value we are checking
		 *
		 * @return bool 		Result of check
		 */
		public static function ValidateAlphanumeric($url)
		{
			//Allow alphanumeric, /, \ - and _ only.
			$regex = '/^[a-zA-Z0-9]*$/';
			return preg_match($regex, $url);
		}

		/**
		 * Check that the username entered contains valid characters we support
		 *
		 * @param int value		Value we are checking
		 *
		 * @return bool 		Result of check
		 */
		public static function ValidateUsername($url)
		{
			//Allow alphanumeric, - and _ only.
			$regex = '/^[a-zA-Z0-9-_]*$/';
			return preg_match($regex, $url);
		}

		/**
		 * Check that the password entered contains valid characters we support
		 *
		 * @param int value		Value we are checking
		 *
		 * @return bool 		Result of check
		 */
		public static function ValidatePassword($url)
		{
			//Allow alphanumeric, @, - and _ only.
			$regex = '/^[a-zA-Z0-9-_@]*$/';
			return preg_match($regex, $url);
		}
	}

?>