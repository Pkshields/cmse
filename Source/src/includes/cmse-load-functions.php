<?PHP
/**
 * This file holds all the functions used on the initial load script cmse-load.php
 *
 * @author Paul Shields - Pkshields.com
 */
	
	//Include required files
	include_once(CMSE_ABSPATH . 'includes/3rd/klogger.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-db.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-userauth.php');


	/**
	 * Generate the logger class & file 
	 *
	 * @return bool		Result
	 */
	function GenerateLogger($logDirectory, $severity)
	{
		global $g_cmseLog;

		//If we already have the logger created, great!
		if (isset($g_cmseLog))
			return true;

		//Else, generate the logger
		try
		{
			$g_cmseLog = new KLogger($logDirectory, $severity);
			$g_cmseLog->logDebug("Logger generated");
			return true;
		}
		catch (Exception $e)
		{
			return false;
		}

		//Just in case.
		return false;
	}

	/**
	 * Create the CMSE DB (if necessary) 
	 *
	 * @return bool		Result
	 */
	function GenerateDatabase()
	{
		global $g_cmseDB;
		global $g_cmseLog;

		//If we already have the database created, great!
		if (isset($g_cmseDB))
			return true;

		//Else, generate the database
		try
		{
			$g_cmseDB = new CMSEDB();
			$g_cmseLog->logDebug("Database generated");
			return true;
		}
		catch (Exception $e)
		{
			global $g_cmseLog;
			$g_cmseLog->logAlert("Could not instanciate the database.");
			return false;
		}

		//Just in case.
		return false;
	}

	/**
	 * Create the CMSE User object for resigtration and user checking (if necessary) 
	 *
	 * @return bool		Result
	 */
	function GenerateUserObject()
	{
		global $g_cmseUser;
		global $g_cmseLog;

		//If we already have the user object created, great!
		if (isset($g_cmseUser))
			return true;

		//Else, generate the user object
		try
		{
			$g_cmseUser = new CMSEUserAuth();
			$g_cmseLog->logDebug("User Object generated");
			return true;
		}
		catch (Exception $e)
		{
			global $g_cmseLog;
			$g_cmseLog->logAlert("Could not instanciate the user object.");
			return false;
		}

		//Just in case.
		return false;
	}

?>