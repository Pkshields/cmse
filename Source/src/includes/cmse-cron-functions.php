<?PHP
/**
 * This file holds the functions used in the CMSE cron script
 *
 * @author Paul Shields - Pkshields.com
 */

	/**
	 * Clear database of old auth keys
	 */
	function PurgeAuthTable()
	{
		global $g_cmseDB;
		global $g_cmseLog;

		//Delete all auths from the table that are older tahn 1 month old
		$query = 'DELETE FROM ' . DB_PREFIX . 'userauth WHERE < NOW() - INTERVAL 4 WEEK';
		$params = array();

		try
		{
			$g_cmseDB->CRUDQuery($query, $params);
		}
		catch (Exception $e)
		{
			echo 'Error running the purge query. Exception! ' . $e->getMessage();
			$g_cmseLog->logAlert("Error running the purge query. Exception! " . $e->getMessage());
			return;
		}

		$g_cmseLog->logNotice("PurgeAuthTable ran in the Cron");
		echo 'Cron run - Purging Auth Database - successful!';
	}

	/**
	 * Clear all the logs older than the date required to keep
	 */
	function PurgeLogs()
	{
		global $g_cmseDB;
		global $g_cmseLog;

		//Get everything in the log directory
		$dir = CMSE_ABSPATH . '/log/';
		$files = scandir($dir, 0);

		//Listed in alphabetical order, remove, keeping the number of files told to keep in the setting
		$count = count($files) - LOG_PURGE_LENGTH;
		for ($i = 2; $i < $count; $i++)
		{
			//If we have read write on the file, then we delete the file. Else, throw errors
			if (is_readable($dir . $files[$i]))
				unlink($dir . $files[$i]);
			else
			{
				echo "Could not remove log file " . $files[$i] . ".";
				$g_cmseLog->logAlert("Could not remove log file " . $files[$i] . ".");
			}
		}

		$g_cmseLog->logNotice("PurgeLogs ran in the Cron");
		echo 'Cron run - Purge Logs - successful!';
	}

?>