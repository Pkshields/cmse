<?PHP 
/**
 * This file holds the CMSE session management code
 * Based upon code from: http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
 *
 * @author Treehouse?
 * @author Paul Shields - Pkshields.com
 */
	include_once(CMSE_ABSPATH . 'includes/cmse-tools.php');

	/**
	 * CMSE session manager
	 */
	class CMSESession
	{
		/**
		 * Securely start the session
		 */
		static function StartSession()
		{
			global $g_cmseLog;

			//Set the cookie name
			session_name('cmse_session');

			//Set the domain to default to the current domain.
			$domain = '.' . $_SERVER['SERVER_NAME'];

			//Set SSL level
			$https = isset($_SERVER['HTTPS']);

			//Set session cookie options
			//NOTE: DOESN'T WORK ON LOCALHOST: http://www.navioo.com/php/docs/function.session-set-cookie-params.php
			if (!CMSETools::IsLocalhost())
				session_set_cookie_params(0, '/', $domain, $https, true);
			session_start();

			//Make sure the session hasn't expired, and destroy it if it has
			if(self::ValidateSession())
			{
				//Check to see if the session is new or a hijacking attempt
				if(!self::PreventHijacking())
				{
					//Reset session data and regenerate id
					$_SESSION = array();
					self::RegenerateSession();
					$_SESSION['IPAddress'] = $_SERVER['REMOTE_ADDR'];
					$_SESSION['UserAgent'] = $_SERVER['HTTP_USER_AGENT'];
					$g_cmseLog->logDebug("Session hijack attempt. IP Address: " . CMSETools::GetUserIP());
				}
			}
			else
			{
				$g_cmseLog->logDebug("Session reset, not valid.");
				$_SESSION = array();
				session_destroy();
				session_start();
			}
		}

		/**
		 * Prevent anyone from hijacking this session
		 *
		 * @return bool 	Has the session been hijacked?
		 */
		static protected function PreventHijacking()
		{
			
			if(!isset($_SESSION['IPAddress']) || !isset($_SESSION['UserAgent']))
				return false;

			if ($_SESSION['IPAddress'] != $_SERVER['REMOTE_ADDR'])
				return false;

			if( $_SESSION['UserAgent'] != $_SERVER['HTTP_USER_AGENT'])
				return false;

			return true;
		}

		/**
		 * Destroy and regenerate a new session
		 */
		static function RegenerateSession()
		{
			//If this session is obsolete it means there already is a new id
			if(isset($_SESSION['OBSOLETE']))
				return;

			//Set current session to expire in 10 seconds
			$_SESSION['OBSOLETE'] = true;
			$_SESSION['EXPIRES'] = time() + 10;

			//Create new session without destroying the old one
			session_regenerate_id(false);

			//Grab current session ID and close both sessions to allow other scripts to use them
			$newSession = session_id();
			session_write_close();

			//Set session ID to the new one, and start it back up again
			session_id($newSession);
			session_start();

			//Now we unset the obsolete and expiration values for the session we want to keep
			unset($_SESSION['OBSOLETE']);
			unset($_SESSION['EXPIRES']);
		}

		/**
		 * Validate that the session has not expired or anyhing
		 *
		 * @return bool 	Is the session valid?
		 */
		static protected function ValidateSession()
		{
			if(isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES']) )
				return false;

			if(isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
				return false;

			return true;
		}
	}