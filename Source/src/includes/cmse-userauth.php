<?PHP 
/**
 * This file holds the CMSE User class, used to check the user and manage access
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the database settings & passHash class
	include_once(CMSE_ABSPATH . 'cmse-settings.php');
	include_once(CMSE_ABSPATH . 'includes/3rd/phpass.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-sessions.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-user.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-tools.php');

	/**
	 * CMSE User and access checking
	 */
	class CMSEUserAuth
	{
		/**
		 * PHPass access
		 *
		 * @access private
		 * @var PasswordHash
		 */
		private $passHash;

		/**
		 * Constructs the varibles needed for user stuff
		 */
		function __construct()
		{
			//Set up the destructor
			register_shutdown_function( array( &$this, '__destruct' ) );

			//Create the password hash variable
			$this->passHash = new PasswordHash(8, TRUE);
		}

		/**
		 * Destructor for the object
		 *
		 * @return bool true
		 */
		function __destruct() {
			return true;
		}

		/**
		 * Register a user into the database
		 *
		 * @param string username		Username to add
		 * @param string password		Password to add
		 * @param string email			Email to add
		 *
		 * @return bool true
		 */
		function Register($username, $password, $email)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Check that we have permission to register new users
			if (CMSE_REGENABLED)
			{
				//Hash the password for the database
				$passwordHash = $this->passHash->HashPassword($password);

				//Set up the MySQL query and the parameters
				$query = 'INSERT INTO ' . DB_PREFIX . 'users (Username,Password,Email,RegDate) VALUES(:uname,:pword,:email,NOW())';
				$params = array(':uname'=>$username,
	                  			  ':pword'=>$passwordHash,
	                  			  ':email'=>$email);

				//Execute the query
				try
				{
					$g_cmseDB->Query($query, $params);
					$g_cmseLog->logNotice("User " . $username . " registered.");
				}
				catch (Exception $e)
				{
					$g_cmseLog->logAlert("Could not register user " . $username . " to the database.");
					return false;
				}

				return true;
			}

			//Else, this method should never have been called. Log
			$g_cmseLog->logAlert("SECURITY WARNING: Someone tried to access the Register function while registration disabled. IP Address: " . CMSETools::GetUserIP());
			return false;
		}

		/**
		 * Check if a user exists with a username
		 *
		 * @param string username		Username to add
		 *
		 * @return bool If the user exists or not
		 */
		function CheckIfUserExists($username)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Set up the MySQL query and the parameters
			$query = 'SELECT * FROM ' . DB_PREFIX . 'users WHERE Username = :uname';
			$params = array(':uname'=>$username);

			//Execute the query
			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//IF we have results, then the user exists
				if (count($results) > 0)
					return true;

			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Query failed, assumed user " . $username . " does not exist in the database.");
				return false;
			}

			return false;
		}

		/**
		 * Check if a user exists with a username
		 *
		 * @param string username		Username to check
		 * @param string password		Password to check
		 *
		 * @return bool 	If the user gets logged in or not
		 */
		function LoginUser($username, $password)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Set up the MySQL query and the parameters for getting the user with this username
			$query = 'SELECT * FROM ' . DB_PREFIX . 'users WHERE Username = :uname';
			$params = array(':uname'=>$username);

			//Execute the query
			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//If we have results, then the user exists
				if (count($results) > 0)
				{
					//Get the password hash from the database and check it
					$truePass = $results[0]['Password'];
					
					if ($this->passHash->CheckPassword($password, $truePass))
					{
						//Success
						//Create the CMSEUser object to hold in the session
						$user = new CMSEUser($results[0]);

						//Set up the cookie and session info
						$this->SetCookie($user);

						//Log.
						$g_cmseLog->logNotice("User " . $username . " has logged in.");

						return true;
					}
					else
						return false;
				}

			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Query failed, assumed user " . $username . " does not exist in the database.");
				return false;
			}

			return false;
		}

		/**
		 * Check if a user is logged in
		 *
		 * @param string password		Password to check
		 *
		 * @return bool If the user is logged in or not
		 */
		function AuthUser($forceCookie = false)
		{
			global $g_cmseLog;

			//1st - check that cookie is set. If not, then don't even try, just go to login screen
			if (!isset($_COOKIE['cmse_01c']))
			{
				$g_cmseLog->logDebug("Auth failed due to missing cookie.");
				CMSESession::RegenerateSession();	//Security
				return false;
			}

			//2nd - see if there is any data held in the session
			//But only run this check if forceCookie isn't enabled
			if (!$forceCookie)
			{
				if (isset($_SESSION['cmse_cookie']) && is_object($_SESSION['cmse_user']))
				{
					//Data is in the session. Ensure it is valid
					if ($_SESSION['cmse_cookie'] == $_COOKIE['cmse_01c'])
					{
						$g_cmseLog->logDebug("Auth succeeded via session. User: " . $_SESSION['cmse_user']->Username);
						return true;
					}
				}
			}

			//3rd - Session failed - try the cookie method
			global $g_cmseDB;

			//Try to get a row from the database with the info from the cookie
			$parsedString = explode('|', $_COOKIE['cmse_01c']);
			$query = 'SELECT * FROM ' . DB_PREFIX . 'userauth WHERE UserID = :uid AND AuthKey = UNHEX(:akey)';
			$params = array(':uid'=>$parsedString[0],
							':akey'=>$parsedString[1]);

			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//If we have results, then the user auth code exists
				if (count($results) > 0)
				{
					//Result exists, check that the Active bool is true
					if ($results[0]['Active'] == "1")
					{
						//Get the user info and shove it into a user object
						$query = 'SELECT * FROM ' . DB_PREFIX . 'users WHERE ID = :uid';
						$params = array(':uid'=>$parsedString[0]);
						$results = $g_cmseDB->Query($query, $params);
						$user = new CMSEUser($results[0]);

						//Exists in the database, fill out the session info (with a new AuthKey)
						CMSESession::RegenerateSession();	//Security
						$this->SetCookie($user, $parsedString[1]);

						$g_cmseLog->logDebug("Auth succeeded via cookie. User: " . $_SESSION['cmse_user']->Username);
						return true;
					}
					//Else the result has been used before. Clear the session! Deny entry!
					else
					{
						$g_cmseLog->logDebug("Auth failed due to cookie being used before, auth key deactivated. Attack! IP Address: " . CMSETools::GetUserIP());
						CMSESession::RegenerateSession();	//Security

						//Delete all sessions in the database for this user
						$query = 'DELETE FROM ' . DB_PREFIX . 'userauth WHERE UserID = :uid';
						$params = array(':uid'=>$parsedString[0]);

						//Delete cookie too
						setcookie('cmse_01c', $_COOKIE['cmse_01c'], time() - 1000, '/');

						try
						{
							$g_cmseDB->Query($query, $params);
						}
						catch (Exception $e)
						{
							$g_cmseLog->logAlert("Query failed, could not remove all AuthKeys for user " . $user->ID . " from the database.");
						}

						return false;
					}
				}
			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Query failed, assumed user auth for " . $username . " does not exist in the database.");
				return false;
			}

			//Nothing worked, not logged in
			$g_cmseLog->logDebug("Auth failed due to nothing else working.");
			return false;
		}

		/**
		 * Logout a user that already exists
		 */
		function LogoutUser()
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Delete all sessions in the database for this user
			$query = 'DELETE FROM ' . DB_PREFIX . 'userauth WHERE UserID = :uid';
			$params = array(':uid'=>$_SESSION['cmse_user']->ID);

			try
			{
				$g_cmseDB->Query($query, $params);
			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Query failed, could not remove all auth sessions for user " . $_SESSION['cmse_user']->ID . " in the database.");
			}

			//Remvoe all traces to the current session
			CMSESession::RegenerateSession();	//Security

			//Delete cookie too
			setcookie('cmse_01c', '', time() - 1000, '/');
		}

		/**
		 * Set a new auth code in the database for a user
		 *
		 * @param CMSEUser user			User to store in the session
		 * @param string currentKey		Current key stored to change (if applicable)
		 */
		protected function SetCookie($user, $currentKey = '')
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Success
			//Calculate a random 128 bit number in hex form for auth purposes
			$bytes = $this->Generate128BitKey();
			$hex = bin2hex($bytes);

			//Fix for setting cookies in localhost. cookie will only set if domain has 2 dots in it.
			$cookieDomain = (CMSETools::IsLocalhost() ? null : CMSE_COOKIEDOMAIN);

			//Setup cookie part
			$storedCookie = $user->ID . '|' . $hex;
			setcookie('cmse_01c', $storedCookie, time() + (86400 * 7), '/', $cookieDomain, null, true); //86400 = 1 day

			//Setup database part
			$query = 'INSERT INTO ' . DB_PREFIX . 'userauth (UserID,AuthKey,Date) VALUES(:uid,UNHEX(:akey),NOW())';
			$params = array(':uid'=>$user->ID,
							':akey'=>$hex);

			try
			{
				$g_cmseDB->Query($query, $params);
			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Query failed, could not add auth code for " . $user->Username . " to the database.");
			}

			//If we are replacing a currrent key, set the old key to not active
			if ($currentKey != '')
			{
				$query = 'UPDATE ' . DB_PREFIX . 'userauth SET Active = FALSE WHERE UserID = :uid AND AuthKey = UNHEX(:akey)';
				$params = array(':uid'=>$user->ID,
								':akey'=>$currentKey);

				try
				{
					$g_cmseDB->Query($query, $params);
				}
				catch (Exception $e)
				{
					$g_cmseLog->logAlert("Query failed, could not deactivate auth code for code " . $currentKey . " to the database.");
				}
			}

			//Start sessions securely & store stuff in them
			$_SESSION['cmse_cookie'] = $storedCookie;
			$_SESSION['cmse_user'] = $user;
		}

		/**
		 * Generate a random 128 bit key
		 *
		 * @return string 	A random 128 bit key
		 */
		protected function Generate128BitKey()
		{
			$str = '';
			for ($i = 0; $i != 16; ++$i) {
			    $str .= chr(mt_rand(0, 255));
			}
			return $str;
		}
	}

?>