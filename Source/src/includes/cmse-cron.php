<?PHP
/**
 * This file holds runs the cron functions from an actual cron
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework and cron functions
	include_once('cmse-load.php');
	include_once('cmse-cron-functions.php');

	//Run the purge users method
	PurgeAuthTable();
	PurgeLogs();
?>