<?PHP 
/**
 * This file holds the CMSE User class, holding user data while someone is logged in
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the database settings & passHash class
	include_once(CMSE_ABSPATH . 'cmse-settings.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-pagedata.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-validator.php');

	/**
	 * CMSE User and access checking
	 */
	class CMSEPages
	{
		/**
		 * Add a page into the database
		 *
		 * @param string title			Title of the page
		 * @param string url			URL of the page
		 * @param string content		Content of the page
		 *
		 * @return array 				List of errors
		 */
		public static function AddPage($title, $url, $content)
		{
			//Default to registration page, no error
			$error = array();

			//Submit is set, we are creating a new page!
			//Get all the data and trim it, error checking along the way
			if (!empty($title))
				$title = trim($title);
			else
				$error[] = "No title was supplied";

			if (!empty($url))
				$url = trim($url);
			else
				$error[] = "No URL was supplied";

			if (!empty($content))
				$content = trim($content);
			else
				$error[] = "No post content was supplied";

			//If we still don't have an error
			if (empty($error))
			{
				//Add the post to the database
				global $g_cmseDB;
				global $g_cmseLog;

				//URL checks
				//First, remove the trailing slash
				$url = rtrim($url, '/');

				//Check that it holds the valid characters
				if (!CMSEValidator::ValidateURL($url))
					$error[] = "URL contains unsupported characters.";

				//Check that the URL doesn't already exist in the database
				$query = 'SELECT ID FROM ' . DB_PREFIX . 'pages WHERE URL = :url';
				$params = array(':url'=>$url);

				//Execute the query
				try
				{
					$results = $g_cmseDB->Query($query, $params);

					//If we have results, then the page exists
					if (count($results) != 0)
						$error[] = "URL already exists in the database";

				}
				catch (Exception $e)
				{
					$g_cmseLog->logAlert("Query failed, URL " . $url . " caused the failure.");
					$error[] = "Query failed";
				}

				//If we still don't have an error
				if (empty($error))
				{
					//Set up the MySQL query and the parameters
					$query = 'INSERT INTO ' . DB_PREFIX . 'pages (Title,Content,URL,UserID,Date) VALUES(:title,:content,:url,:uid,NOW())';
					$params = array(':title'=>$title,
		                  			':content'=>$content,
		                  			':url'=>$url,
		                  			':uid'=>$_SESSION['cmse_user']->ID);

					//Execute the query
					try
					{
						$g_cmseDB->Query($query, $params);
						$g_cmseLog->logNotice("Page " . $title . " add to the database.");
						header('Location: pages.php?a=created');
					}
					catch (Exception $e)
					{
						$g_cmseLog->logAlert("Could not create the post , title" . $title . " to the database.");
						$error[] = "Query failed to create the new page.";
					}
				}
			}

			return $error;
		}

		/**
		 * Edit a page in the database
		 *
		 * @param int id				ID of the page
		 * @param string title			Title of the page
		 * @param string url			URL of the page
		 * @param string content		Content of the page
		 *
		 * @return array 				List of errors
		 */
		public static function EditPage($id, $title, $url, $content)
		{
			//Default to registration page, no error
			$error = array();

			//Submit is set, we are creating a new page!
			//Get all the data and trim it, error checking along the way
			if (!empty($id))
				$id = $id;
			else
				$error[] = "No page ID was supplied";

			if (!empty($title))
				$title = trim($title);
			else
				$error[] = "No title was supplied";

			if (!empty($url))
				$url = trim($url);
			else
				$error[] = "No URL was supplied";

			if (!empty($content))
				$content = trim($content);
			else
				$error[] = "No post content was supplied";

			//If we still don't have an error
			if (empty($error))
			{
				//Add the post to the database
				global $g_cmseDB;
				global $g_cmseLog;

				//URL checks
				//First, remove the trailing slash
				$url = rtrim($url, '/');

				//Check that it holds the valid characters
				if (!CMSEValidator::ValidateURL($url))
					$error[] = "URL contains unsupported characters.";

				//If we still don't have an error
				if (empty($error))
				{
					//Set up the MySQL query and the parameters
					$query = 'UPDATE ' . DB_PREFIX . 'pages  SET Title=:title, Content=:content, URL=:url, UserID=:uid, Date=NOW() WHERE ID=:id';
					$params = array(':title'=>$title,
		                  			':content'=>$content,
		                  			':url'=>$url,
		                  			':uid'=>$_SESSION['cmse_user']->ID,
		                  			':id'=>$id);

					//Execute the query
					try
					{
						$g_cmseDB->Query($query, $params);
						header('Location: pages.php?a=updated');
					}
					catch (Exception $e)
					{
						$g_cmseLog->logAlert("Could not update the post, title" . $title . " to the database.");
						$error[] = "Query failed to update the existing page.";
					}
				}
			}

			return $error;
		}

		/**
		 * Get the list of pages from the database
		 *
		 * @return array 	Array of pages in the database
		 */
		public static function GetPages()
		{
			global $g_cmseDB;
			global $g_cmseLog;

			$pages = array();

			//Get the list of pages from the database
			$query = 'SELECT * FROM ' . DB_PREFIX . 'pages';
			$params = array();

			//Execute the query
			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//If we have results, then the page exists
				if (count($results) != 0)
				{
					//Next get the list of users usernames in the database
					$query = 'SELECT ID, Username FROM ' . DB_PREFIX . 'users';
					$results2 = $g_cmseDB->OrderedQuery($query, $params);

					//If we have results, then the users exist too!
					if (count($results2) != 0)
					{
						foreach($results as $row)
						{
							//Add the page from the database, along with the user's Username
					        $pages[] = new CMSEPageData($row, $results2[$row['UserID']][0]['Username']);
					    }
					}

					return $pages;
				}

			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Failed to get list of pages (or users) from the database.");
				return $pages;
			}
		}

		/**
		 * Get a specific page's data from the database
		 *
		 * @param int pageID		ID of the page to get
		 *
		 * @return array 			Page data
		 */
		public static function GetPageByID($pageID)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			$pages = array();

			//Get data for this specific pageIF
			$query = 'SELECT * FROM ' . DB_PREFIX . 'pages WHERE ID = :pageid';
			$params = array(':pageid'=>$pageID);

			//Execute the query
			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//If we have results, then the page exists
				if (count($results) != 0)
				{
					return $results[0];
				}

			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Failed to get list of pages (or users) from the database.");
				return null;
			}

			return null;
		}

		/**
		 * Get a specific page's data from the database
		 *
		 * @param int url			URL of the page to get
		 *
		 * @return array 			Page data
		 */
		public static function GetPageByURL($url)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			//Ensure the URL passed in is valid
			if (!CMSEValidator::ValidateURL($url))
			{
				$g_cmseLog->logAlert("Requested page for an invlaid URL. Something bad is happening.");
				return null;
			}

			//Get data for this specific page
			$query = 'SELECT * FROM ' . DB_PREFIX . 'pages WHERE URL = :url';
			$params = array(':url'=>$url);

			//Execute the query
			try
			{
				$results = $g_cmseDB->Query($query, $params);

				//If we have results, then the page exists
				if (count($results) != 0)
				{
					return $results[0];
				}

			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Failed to get list of pages (or users) from the database.");
				return null;
			}

			return null;
		}


		/**
		 * Delete a specific page from the database
		 *
		 * @param int pageID		ID of the page to remove
		 */
		public static function DeletePage($pageID)
		{
			global $g_cmseDB;
			global $g_cmseLog;

			$pages = array();

			//Create query to delete page form the database
			$query = 'DELETE FROM ' . DB_PREFIX . 'pages WHERE id = :pageid';
			$params = array(':pageid'=>$pageID);

			//Execute the query
			try
			{
				$g_cmseDB->Query($query, $params);
				$g_cmseLog->logNotice("Page " . $pageID . " removed to the database.");
			}
			catch (Exception $e)
			{
				$g_cmseLog->logAlert("Failed to remove the page with id " . $pageID . " from the database.");
				return null;
			}
		}
	}
