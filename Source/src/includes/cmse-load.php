<?PHP
/**
 * This script is used to ensure the framework is loaded at the start of every page
 *
 * @author Paul Shields - Pkshields.com
 */

	//Includes suck in PHP apparently. Fix.
	define('CMSE_ABSPATH', dirname(dirname(__FILE__)) . '/');

	//Include this scripts functions & settings
	include_once (CMSE_ABSPATH . "includes/cmse-load-functions.php");
	include_once (CMSE_ABSPATH . "cmse-settings.php");
	include_once (CMSE_ABSPATH . "includes/cmse-cron-functions.php");

	//Generate the logger file
	GenerateLogger(LOG_DIR, LOG_LEVEL);

	//Generate the database, if necessary
	GenerateDatabase();

	//Generate the user object, if necessary
	GenerateUserObject();

	//Start the sessions, so no one else has to
	CMSESession::StartSession();

	//If we have fake cron enabled
	if (CMSE_CRON)
	{
		//Run the cron 1 in roughly every 1000 page loads
		$random = rand (0, 1000);
		if ($random == 0)
		{
			PurgeAuthTable();
			PurgeLogs();
		}
	}
?>