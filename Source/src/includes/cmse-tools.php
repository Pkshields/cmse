<?PHP 
/**
 * This file holds a bunch of tools for CMSE to use
 *
 * @author Paul Shields - Pkshields.com
 */
	include_once(CMSE_ABSPATH . "includes/cmse-sessions.php");

	/**
	 * CMSE Tools
	 */
	class CMSETools
	{

		/**
		 * Check if the system is currently running under localhost 
		 *
		 * @return bool			Is localhost?
		 */
		static function IsLocalhost()
		{
			return $_SERVER['HTTP_HOST'] == 'localhost';
		}

		/**
		 * Get the current IP address of a user
		 *
		 * @return string 			User IP
		 */
		static function GetUserIP()
		{
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				return $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				return $_SERVER['REMOTE_ADDR'];
			}
		}
	}

?>