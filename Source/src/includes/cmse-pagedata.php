<?PHP 
/**
 * This file holds the CMSE PageData class, holding information associated to each page
 *
 * @author Paul Shields - Pkshields.com
 */

	/**
	 * CMSE Page Data
	 */
	class CMSEPageData
	{
		/**
		 * Page ID
		 *
		 * @access private
		 * @var int
		 */
		private $ID;

		/**
		 * Title of the page
		 *
		 * @access private
		 * @var string
		 */
		private $Title;

		/**
		 * Content of the page
		 *
		 * @access private
		 * @var LongText
		 */
		private $Content;

		/**
		 * URL
		 *
		 * @access private
		 * @var string
		 */
		private $URL;

		/**
		 * User ID
		 *
		 * @access private
		 * @var int
		 */
		private $UserID;

		/**
		 * Date of posting
		 *
		 * @access private
		 * @var DateTime
		 */
		private $Date;

		/**
		 * Username of poster
		 *
		 * @access private
		 * @var string
		 */
		private $Username;

		/**
		 * Constructs the varibles needed for user stuff
		 */
		function __construct($userData, $username = '')
		{
			//Set all the data from the result from the database
			$this->ID = $userData['ID'];
			$this->Title = $userData['Title'];
			$this->Content = $userData['Content'];
			$this->URL = $userData['URL'];
			$this->UserID = $userData['UserID'];
			$this->Date = $userData['Date'];
			$this->Username = $username;
		}

		/**
		 * Getter for any object, no setter
		 *
		 * @return ? 	property wanted
		 */
		public function __get($property)
		{
			if (property_exists($this, $property))
			{
				return $this->$property;
			}
		}
	}