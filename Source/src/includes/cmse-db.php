<?PHP 
/**
 * This file holds the CMSE DB class, used to access the database
 *
 * @author Paul Shields - Pkshields.com
 */

	//Inlcude the database settings
	include_once(CMSE_ABSPATH . "cmse-settings.php");

	/**
	 * CMSE Database Access
	 */
	class CMSEDB
	{
		/**
		 * PDO Database Object itself
		 *
		 * @access private
		 * @var PDO
		 */
		private $db;

		/**
		 * Constructs the database connection
		 */
		function __construct()
		{
			//Set up the destructor
			register_shutdown_function( array( &$this, '__destruct' ) );

			//Connect to the database & store it
			$dbsetup = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8';
			$this->db = new PDO($dbsetup, DB_USER, DB_PASSWORD);
		}

		/**
		 * Destructor for the object
		 *
		 * @return bool true
		 */
		function __destruct()
		{
			return true;
		}

		/**
		 * Simple query to the database
		 *
		 * @param string query		Query to execute
		 * @param array params		Array of parameters to pass in
		 *
		 * @return bool true
		 */
		function Query($query, $params = null)
		{
			if ($params == null)
			{
				$stmt = $this->db->query($query);		//E.G. 'SELECT * FROM myTable'
			}
			else
			{
				$stmt = $this->db->prepare($query);	//E.G. 'SELECT * FROM myTable WHERE id = :id'
				$stmt->execute($params);				//E.G. array('id' => $id)
			}

			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $results;
		}

		/**
		 * Simple query to the database, sans any returns
		 *
		 * @param string query		Query to execute
		 * @param array params		Array of parameters to pass in
		 *
		 * @return bool true
		 */
		function CRUDQuery($query, $params = null)
		{
			if ($params == null)
			{
				$stmt = $this->db->query($query);		//E.G. 'SELECT * FROM myTable'
			}
			else
			{
				$stmt = $this->db->prepare($query);	//E.G. 'SELECT * FROM myTable WHERE id = :id'
				$stmt->execute($params);				//E.G. array('id' => $id)
			}
		}

		/**
		 * Query the database with the returned rows being ordered by the first param
		 *
		 * @param string query		Query to execute
		 * @param array params		Array of parameters to pass in
		 *
		 * @return bool true
		 */
		function OrderedQuery($query, $params)
		{
			if ($params == null)
			{
				$stmt = $this->db->query($query);		//E.G. 'SELECT * FROM myTable'
			}
			else
			{
				$stmt = $this->db->prepare($query);	//E.G. 'SELECT * FROM myTable WHERE id = :id'
				$stmt->execute($params);				//E.G. array('id' => $id)
			}

			$results = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
			return $results;
		}
	}

?>