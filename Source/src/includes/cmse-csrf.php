<?PHP 
/**
 * This file holds the CMSE CSRF class, used to protect against CSRF attacks on forms
 *
 * @author Paul Shields - Pkshields.com
 */
	include_once(CMSE_ABSPATH . "includes/cmse-sessions.php");

	/**
	 * CMSE CSRF protection
	 */
	class CMSECSRF
	{
		/**
		 * Maximum amout of seconds we can keep an ID alive for
		 *
		 * @access private
		 * @var int
		 */
		static private $maxTime = 300;

		/**
		 * Grace time over the maxTime limit that we allow a token through
		 * Fixes possible problem where token is issued to form that is aboue to expire
		 *
		 * @access private
		 * @var int
		 */
		static private $graceTime = 300;

		/**
		 * Generate a CSRF token for a specific form 
		 *
		 * @param string formID		ID of the form to generate the CSRF token for
		 *
		 * @return string 			CSRF Token
		 */
		static function GenerateCSRF($formID)
		{
			//If we have a token set for this form and it isn't expired, just use that
			if (isset($_SESSION['csrf_' . $formID]) && isset($_SESSION['csrf_' . $formID . '_time']))
			{
				$tokenAge = time() - $_SESSION['csrf_' . $formID . '_time'];

				if ($tokenAge < self::$maxTime)
					return $_SESSION['csrf_' . $formID];
			}

			//Else, generate a new token
			$token = self::GenerateCSRFToken();
			$_SESSION['csrf_' . $formID] = $token;
			$_SESSION['csrf_' . $formID . '_time'] = time();

			return $token;
		}

		/**
		 * Authenticate a CSRF token for a specific form 
		 *
		 * @param string token		Token to authenticate
		 * @param string formID		ID of the form to generate the CSRF token for
		 *
		 * @return string 			CSRF Token
		 */
		static function AuthenticateCSRFToken($token, $formID)
		{
			//Ensure that we have a token set for this form and it isn't expired
			if (isset($_SESSION['csrf_' . $formID]) && isset($_SESSION['csrf_' . $formID . '_time']))
			{
				$tokenAge = time() - $_SESSION['csrf_' . $formID . '_time'];

				if ($tokenAge < self::$maxTime + self::$graceTime)
				{
					//Now we check that the token is valid
					return ($_SESSION['csrf_' . $formID] == $token);
				}
			}

			return false;
		}

		/**
		 * Generate a random, unique CSRF token
		 *
		 * @return string CSRF Token
		 */
		private static function GenerateCSRFToken()
		{
			return md5(uniqid(rand(), TRUE));
		}
	}

?>