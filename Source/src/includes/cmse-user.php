<?PHP 
/**
 * This file holds the CMSE User class, holding user data while someone is logged in
 *
 * @author Paul Shields - Pkshields.com
 */

	/**
	 * CMSE User and access checking
	 */
	class CMSEUser
	{
		/**
		 * User ID
		 *
		 * @access private
		 * @var int
		 */
		private $ID;

		/**
		 * Username
		 *
		 * @access private
		 * @var string
		 */
		private $Username;

		/**
		 * Constructs the varibles needed for user stuff
		 */
		function __construct($userData)
		{
			//Set up the destructor
			register_shutdown_function( array( &$this, '__destruct' ) );

			//Set all the data from the result from the database
			$this->ID = $userData['ID'];
			$this->Username = $userData['Username'];
		}

		/**
		 * Destructor for the object
		 *
		 * @return bool true
		 */
		function __destruct() {
			return true;
		}

		/**
		 * Getter for any object, no setter
		 *
		 * @return ? 	property wanted
		 */
		public function __get($property)
		{
			if (property_exists($this, $property))
			{
				return $this->$property;
			}
		}
	}