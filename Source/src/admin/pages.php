<?PHP
/**
 * Page listing all pages in the database
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework
	include_once('../includes/cmse-load.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-pages.php');

	global $g_cmseUser;

	//If user isn't logged in, redirect to the login page
	if (!$g_cmseUser->AuthUser())
		header('Location: ../login.php');

	//IF the user wants to logout, logout
	if (isset($_GET['a']))
	{
		if ($_GET['a'] == 'created')
			$message ='New page Successfully created!';
		else if ($_GET['a'] == 'updated')
			$message ='Page Successfully updated!';
		else if ($_GET['a'] == 'deleted')
			$message ='Page Successfully deleted!';
		else if ($_GET['a'] == 'invalid')
			$message ='Page ID is invalid';
	}

	//Get the list of pages from the database
	$pages = CMSEPages::GetPages();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="theme/style.admin.css" />
	<link rel="stylesheet" type="text/css" href="theme/style.admintable.css" />
</head>
<body>
	<div id="header">
		<div>CMSE Admin Panel - <a href="<?PHP echo CMSE_PROTOCOL . CMSE_URL; ?>"><?PHP echo CMSE_SITENAME; ?></a></div>
		<div id="left">Hello <?PHP echo $_SESSION['cmse_user']->Username ?> - <a href="../login.php?a=logout">Logout</a></div>
	</div>
	<div id="links">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="pages.php">Pages</a></li>
			<dl>
				<dt><a href="addpage.php">Add Page</a></dt>
				<dt><a href="pages.php">Edit Pages</a></dt>
			</dl>
			<li><a href="files.php">Files</a></li>
			<li><a href="#">Settings</a></li>
		</ul>
	</div>
	<div id="content">
		<h2>Pages</h2>
		<div>
<?PHP
	if (isset($message))
	{
		echo '<p class="success" style="text-align: center;">' . $message . '</p>';
	}

	$pageCount = count($pages);
	if ($pageCount != 0)
	{
?>
			<div class="themetable">
				<table style="border:0; width:100%; text-align:center;" cellpadding="3" cellspacing="3">
					<thead>
						<tr>
							<th style="width: 35%;">Page</th>
							<th>Author</th>
							<th>Edit Date</th>
							<th>Functions</th>
						</tr>
					</thead>
					<tbody>
<?PHP
		for ($i = 0; $i < $pageCount; $i++) 
		{
			echo '<tr' . ($i % 2 == 1 ? ' class="alt"' : '') . '>';
			echo '<td><a href="' . CMSE_PROTOCOL . CMSE_URL . '/' . $pages[$i]->URL . '">' . $pages[$i]->Title . '</a></td>';
			echo '<td>' . $pages[$i]->Username . '</td>';
			echo '<td>' . $pages[$i]->Date . '</td>';
			echo '<td><a href="addpage.php?p=' . $pages[$i]->ID . '"><img src="theme/edit.png" style="width:25px" /></a> <a href="deletepage.php?p=' . $pages[$i]->ID . '"><img src="theme/delete.png" style="width:25px" /></a></td>';
			echo '</tr>';
		}
?>

					</tbody>
				</table>
			</div>
<?PHP
	}
	else
	{
		echo '<div class="themetable">No pages in the database!</div>';
	}
?>
		</div>
	</div>
</body>
</html>