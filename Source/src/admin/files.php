<?PHP
/**
 * Files page - to add and remove files from the uploads folder - for the admin panel
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework
	include_once('../includes/cmse-load.php');

	global $g_cmseUser;

	//If user isn't logged in, redirect to the login page
	if (!$g_cmseUser->AuthUser())
		header('Location: ../login.php');
 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="theme/style.admin.css" />

</head>
<body>
	<div id="header">
		<div>CMSE Admin Panel - <a href="<?PHP echo CMSE_PROTOCOL . CMSE_URL; ?>"><?PHP echo CMSE_SITENAME; ?></a></div>
		<div id="left">Hello <?PHP echo $_SESSION['cmse_user']->Username ?> - <a href="../login.php?a=logout">Logout</a></div>
	</div>
	<div id="links">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="pages.php">Pages</a></li>
			<li><a href="files.php">Files</a></li>
			<li><a href="#">Settings</a></li>
		</ul>
	</div>
	<div id="content">
		<h2>Manage Files</h2>
		<div>
			<iframe src="../webincludes/elfinder/elfinder.html" style="width: 100%; height: 420px; border: 0;"></iframe>
		</div>
	</div>
</body>
</html>