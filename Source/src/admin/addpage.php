<?PHP
/**
 * Home page for the admin panel
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework
	include_once('../includes/cmse-load.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-pages.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-validator.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-csrf.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-tools.php');

	global $g_cmseUser;
	global $g_cmseLog;

	//If user isn't logged in, redirect to the login page
	if (!$g_cmseUser->AuthUser())
		header('Location: ../login.php');

	//CSRF token ID name thing
	$csrfID = 'addpage';

	//Default to registration page, no error
	$error = array();

	//Get the page id were editing, if applicable
	$id = (isset($_GET["p"]) ? $_GET["p"] : '');

	//Check that the ID is valid
	if (!CMSEValidator::ValidateNum($id))
		header('Location: pages.php?a=invalid');

	//Variables to hold the page data, if applicable
	$title = 'Add Header';
	$url = '';
	$content = '';

	if (isset($_POST["addPageSubmit"]))
	{
		$csrfToken = $_POST["csrftoken"];
		if (CMSECSRF::AuthenticateCSRFToken($csrfToken, $csrfID))
		{
			$title = $_POST["title"];
			$url = $_POST["url"];
			$content = $_POST["pagecontent"];

			if ($id == '')
			{
				//Try to add page to database
				$error = CMSEPages::AddPage($_POST["title"], $_POST["url"], $_POST["pagecontent"]);
			}
			else
			{
				//Try to edit page to database
				$error = CMSEPages::EditPage($id, $_POST["title"], $_POST["url"], $_POST["pagecontent"]);
			}
		}
		else
		{
			$error[] = "An error has occured, please try again";
			$g_cmseLog->logAlert("CMSE Authentication failed! Username: " . $_SESSION['cmse_user']->Username . " Page: " . $csrfID . " IP Address: " . CMSETools::GetUserIP());
		}
	}
	else if ($id != '')
	{
		//Get page data
		$data = CMSEPages::GetPageByID($id);
		
		$title = $data['Title'];
		$url = $data['URL'];
		$content = $data['Content'];
	}

	//Generate CSRF token
	$csrfToken = CMSECSRF::GenerateCSRF($csrfID);
 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="theme/style.admin.css" />

	<script type="text/javascript" src="../webincludes/jquery/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="../webincludes/tinymce/tiny_mce.js"></script>

	<script type="text/javascript">
	tinyMCE.init({
	        // General options
	        mode : "textareas",
	        theme : "advanced",
	        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

	        // Theme options
	        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect,|,hr,removeformat,visualaid,|,sub,sup,|,cite,abbr,acronym,del,ins,attribs",
	        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker",
	        theme_advanced_buttons3 : "tablecontrols,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
	        theme_advanced_toolbar_location : "top",
	        theme_advanced_toolbar_align : "left",
	        theme_advanced_statusbar_location : "bottom",
	        theme_advanced_resizing : true,

	        // Skin options
	        skin : "cirkuit",

	        // File browser integration
	        file_browser_callback : 'elFinderBrowser',

	        // Drop lists for link/image/media/template dialogs
	        template_external_list_url : "js/template_list.js",
	        external_link_list_url : "js/link_list.js",
	        external_image_list_url : "js/image_list.js",
	        media_external_list_url : "js/media_list.js",

	        // Replace values for the template plugin
	        template_replace_values : {
	                username : "Some User",
	                staffid : "991234"
	        },
			
			//Allow the style tag in TinyMCE, for hacks
			valid_children : "+body[style]"
	});

	function elFinderBrowser (field_name, url, type, win) {
		var elfinder_url = '<?PHP echo CMSE_PROTOCOL . CMSE_URL; ?>/webincludes/elfinder/elfinder_tinymce.html';    // use an absolute path!
		tinyMCE.activeEditor.windowManager.open({
			file: elfinder_url,
		    title: 'elFinder 2.0',
		    width: 900,  
		    height: 450,
		    resizable: 'yes',
		    inline: 'yes',    // This parameter only has an effect if you use the inlinepopups plugin!
		    popup_css: false, // Disable TinyMCE's default popup CSS
		    close_previous: 'no'
		}, {
			window: win,
			input: field_name
		});
		return false;
	}
	</script>

</head>
<body>
	<div id="header">
		<div>CMSE Admin Panel - <a href="<?PHP echo CMSE_PROTOCOL . CMSE_URL; ?>"><?PHP echo CMSE_SITENAME; ?></a></div>
		<div id="left">Hello <?PHP echo $_SESSION['cmse_user']->Username ?> - <a href="../login.php?a=logout">Logout</a></div>
	</div>
	<div id="links">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="pages.php">Pages</a></li>
			<dl>
				<dt><a href="addpage.php">Add Page</a></dt>
				<dt><a href="pages.php">Edit Pages</a></dt>
			</dl>
			<li><a href="files.php">Files</a></li>
			<li><a href="#">Settings</a></li>
		</ul>
	</div>
	<div id="content">
		<h2><?PHP echo ($id == '' ? "Add Page" : "Edit Page"); ?></h2>
		<div>
<?PHP
	//If we have errors, echo them
	if (isset($_POST["addPageSubmit"]) && !empty($error))
	{
		foreach ($error as $value)
			echo '<p class="error" style="text-align: center;">' . $value . '</p>';

		echo '<br />';
	}
?>
			<form name="addpageform" id="addpageform" action="<?PHP echo 'addpage.php' . ($id != '' ? '?p=' . $id : ''); ?>" method="post">
				<input class="addpage slightinputborder" type="text" name="title" id="title" value='<?PHP echo $title; ?>' size="50" />
				<label for="url" class="addpage">URL (<?PHP echo CMSE_PROTOCOL . CMSE_URL . '/'; ?>)<input class="addpage slightinputborder" type="text" name="url" id="url" value='<?PHP echo $url; ?>' size="50" /></label>

				<textarea id="pagecontent" name="pagecontent" style="width:100%; height:350px"><?PHP echo $content; ?></textarea>
				<input type="hidden" name="csrftoken" value="<?php echo $csrfToken; ?>" />

				<div style="width: 100%; text-align:right; margin: 12px 0 0 -15px"><input type="submit" name="addPageSubmit" id="addPageSubmit" value="<?PHP echo ($id != '' ? 'Edit Page' : 'Add New Page'); ?>" style="padding:5px" /></div>
			</form>
		</div>
	</div>
</body>
</html>