<?PHP
/**
 * Page listing all pages in the database
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework
	include_once('../includes/cmse-load.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-pages.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-csrf.php');

	global $g_cmseUser;
	global $g_cmseLog;

	//If user isn't logged in, redirect to the login page
	if (!$g_cmseUser->AuthUser())
		header('Location: ../login.php');

	//Get page from the GET
	$pageid = (isset($_GET['p']) ? $_GET['p'] : null );

	if (!CMSEValidator::ValidateNum($pageid))
		header('Location: pages.php?a=invalid');

	//CSRF token ID name thing << we want this to be unique to each page since it's completely open
	$csrfID = 'deletepage' . $pageid;

	//If page is passed in...
	if (!is_null($pageid))
	{
		//... Delete it if required
		if (isset($_GET['d']))
		{
			if ($_GET['d'] == 'y')
			{
				if (isset($_GET["t"]) && CMSECSRF::AuthenticateCSRFToken($_GET["t"], $csrfID))
				{
					CMSEPages::DeletePage($pageid);
					header('Location: pages.php?a=deleted');
				}
				else
				{
					$pageid = null;
					$g_cmseLog->logAlert("CMSE Authentication failed! Username: " . $_SESSION['cmse_user']->Username . " Page: " . $csrfID . " IP Address: " . CMSETools::GetUserIP());
				}
			}	
		}

		//... or check that it exists
		else
		{
			$pagedata = CMSEPages::GetPageByID($pageid);
			if (is_null($pagedata))
				$pageid = -1;
		}
	}

	//Generate CSRF token
	$csrfToken = CMSECSRF::GenerateCSRF($csrfID);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="theme/style.admin.css" />
</head>
<body>
	<div id="header">
		<div>CMSE Admin Panel - <a href="<?PHP echo CMSE_PROTOCOL . CMSE_URL; ?>"><?PHP echo CMSE_SITENAME; ?></a></div>
		<div id="left">Hello <?PHP echo $_SESSION['cmse_user']->Username ?> - <a href="../login.php?a=logout">Logout</a></div>
	</div>
	<div id="links">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="pages.php">Pages</a></li>
			<dl>
				<dt><a href="addpage.php">Add Page</a></dt>
				<dt><a href="pages.php">Edit Pages</a></dt>
			</dl>
			<li><a href="files.php">Files</a></li>
			<li><a href="#">Settings</a></li>
		</ul>
	</div>
	<div id="content">
<?PHP

	if (is_null($pageid))
		echo "<h2 class='error'>No page selected for deletion!</h2>";

	else if ($pageid == -1)
		echo "<h2 class='error'>Page does not exist!</h2>";

	else
	{
?>
		<h2>Page: <?PHP echo $pagedata['Title']; ?></h2>
		<div>
			<p>Are you sure you want to remove page ID <?PHP echo $pagedata['ID']; ?>?</p>
			<p><a href="deletepage.php?p=<?PHP echo $pageid; ?>&d=y&t=<?PHP echo $csrfToken; ?>">Remove page permenentally</a></p>
		</div>
<?PHP
	}
?>
	</div>
</body>
</html>