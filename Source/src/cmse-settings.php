<?PHP
/**
 * Defines the settings for CMSE
 */

	// ** MySQL settings ** //
	
	define('DB_NAME', 'cmse');							//The name of the database
	define('DB_USER', 'root');							//DB Username
	define('DB_PASSWORD', '@mysqlxampp');				//DB Password
	define('DB_HOST', 'localhost');						//DB host URL
	define('DB_PREFIX', 'cmse_');						//Prefix on all db tables

	// ** Logger settings ** //

	define('LOG_DIR', 'C:\Apps\xampp\htdocs\cmse\Project\log');					//Location of the log file
	define('LOG_LEVEL', KLogger::DEBUG);					//Debugger level << INFO
	define('LOG_PURGE_LENGTH', 7);						//Number of days to keep logs before they are purged

	// ** CMSE settings ** //

	define('CMSE_REGENABLED', false);					//Is registration enabled?
	define('CMSE_CRON', true);							//Is fake cron enabled?
	define('CMSE_COOKIEDOMAIN', 'localhost');			//Domain for the cookies
	define('CMSE_URL', 'localhost/cmse/Project');		//Full URL to the instance of CMSE
	define('CMSE_PROTOCOL', 'http://');					//Protocol used in this instance. For use later when HTTPS becomes a thing I should support
	define('CMSE_SITENAME', 'Muzene.com');				//Name of the website we are hosting
	define('CMSE_DEFAULTINDEX', 'home');				//URL of the page we are showing on the front page
	define('CMSE_THEME', 'Default');					//Current theme we are using on the site

	define('CMSE_VERSION', 'v1.0 Alpha 2');				//Current CMSE version
?>