<?PHP
/**
 * User login page
 *
 * @author Paul Shields - Pkshields.com
 */

	//Include the CMSE framwework
	include_once('includes/cmse-load.php');
	include_once(CMSE_ABSPATH . "includes/cmse-validator.php");
	include_once(CMSE_ABSPATH . 'includes/cmse-csrf.php');

	//CSRF token ID name thing
	$csrfID = 'login';

	//IF the user wants to logout, logout
	if (isset($_GET['a']))
		if ($_GET['a'] == 'logout')
			$g_cmseUser->LogoutUser();

	//If user is already logged in, redirect
	if ($g_cmseUser->AuthUser())
		header('Location: admin/');

	//Default to registration page, no error
	$error = array();

	if (isset($_POST["loginSubmit"]))
	{
		$csrfToken = $_POST["csrftoken"];
		if (CMSECSRF::AuthenticateCSRFToken($csrfToken, $csrfID))
		{
			//Check the 2 variables
			//If they are available, trim them, else throw error
			if (!empty($_POST["username"]))
				$username = trim($_POST["username"]);
			else
				$error[] = "No username was supplied";

			if (!empty($_POST["password"]))
				$password = trim($_POST["password"]);
			else
				$error[] = "No password was supplied";

			//If we still don't have an error
			if (empty($error))
			{
				global $g_cmseUser;

				//Validate usrename and password, checking for allowed characters
				if (!CMSEValidator::ValidateUsername($username))
					$error[] = "Username supplied is invalid";

				if (!CMSEValidator::ValidatePassword($password))
					$error[] = "Password supplied is invalid";

				//If we still don't have an error
				if (empty($error))
				{
					//Authenticate login
					if ($g_cmseUser->LoginUser($username, $password))
					{
						//Auth worked!
						header('Location: admin/');
					}
					else
					{
						//Auth failed
						$error[] = "Could not log you in with those details.";
					}
				}
			}
		}
		else
		{
			$error[] = "An error has occured, please try again";
			$g_cmseLog->logAlert("CMSE Authentication failed! Username: " . $_POST["username"] . " Page: " . $csrfID . " IP Address: " . CMSETools::GetUserIP());
		}
	}

	//Generate CSRF token
	$csrfToken = CMSECSRF::GenerateCSRF($csrfID);
 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="admin/theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel - Login</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="admin/theme/style.login.css" />
</head>
<body>
	<div id="container">
		<h2>Login</h2>
<?PHP
	//If we have errors, echo them
	if (isset($_POST["loginSubmit"]) && !empty($error))
	{
		foreach ($error as $value)
			echo '<p class="error">' . $value . '</p>';

		echo '<br />';
	}
?>
		<form name="loginform" id="loginform" action="login.php" method="post">
			<label for="username">Username <br /><input type="text" name="username" id="username" value="" size="20" /></label>
			<label for="password">Password <br /><input type="password" name="password" id="password" value="" size="20" /></label>
			<input type="hidden" name="csrftoken" value="<?php echo $csrfToken; ?>" />
			<input type="submit" name="loginSubmit" id="loginSubmit" value="Log In" />
		</form>
	</div>
</body>
</html>