<?PHP
/**
 * Alpha Sample front page. Temp setup till Themes are implemented
 *
 * @author Paul Shields - Pkshields.com
 */

	//CORE SITE LOCATION
	//Used for site separation, if you want the CMSE framework in a folder spearately from the root of the site
	$siteLocation = '';

	//Include the CMSE framwework
	include_once($siteLocation . 'includes/cmse-load.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-pages.php');
	include_once(CMSE_ABSPATH . 'includes/cmse-validator.php');

	//Get the current page from the URL input
	//If no page is passed in, then get the default page from the settings
	$page = (!empty($_GET['page']) ? $_GET['page'] : CMSE_DEFAULTINDEX);
	$siteURL = CMSE_PROTOCOL . CMSE_URL . '/';
	$themeURL = CMSE_PROTOCOL . CMSE_URL . '/' . $siteLocation .'themes/' . CMSE_THEME . '/';

	//Check that the URL is valid
	if (CMSEValidator::ValidateURL($page))
	{
		//Get the page data
		$pageData = CMSEPages::GetPageByURL($page);

		//Check that the page exists
		if (is_null($pageData))
		{
			//Show 404 page. Either custom for theme or default
			if (file_exists(CMSE_ABSPATH . 'themes/' . CMSE_THEME . '/404.php'))
			{
				include_once(CMSE_ABSPATH . 'themes/' . CMSE_THEME . '/404.php');
			}
			else
			{
				header('HTTP/1.1 404 Not Found');
				$_GET['e'] = 404;
				$title = "404 Not Found";
				$content = "";
			}
		}
		else
		{
			//Great! get the page data
			$title = $pageData['Title'];
			$content = $pageData['Content'];
		}
	}
	else
	{
		//URL error. Fill the page with invalid data
		$title = "Unvalid URL Entered";
		$content = "Don't try that again.";
	}

	//Import the theme index to display on the page
	include_once(CMSE_ABSPATH .'themes/' . CMSE_THEME . '/index.php');
?>