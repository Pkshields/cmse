<?PHP
/**
 * User Register page
 *
 * @author Paul Shields - Pkshields.com
 */
	
	//Include the CMSE framwework & settings
	include_once('includes/cmse-load.php');
	include_once(CMSE_ABSPATH . 'cmse-settings.php');
	include_once(CMSE_ABSPATH . "includes/cmse-validator.php");

	//Default to registration page, no error
	$error = array();

	if (isset($_POST["registerSubmit"]) && CMSE_REGENABLED)
	{
		//Check the 4 variables
		//If they are available, trim them, else throw error
		if (!empty($_POST["username"]))
			$username = trim($_POST["username"]);
		else
			$error[] = "No username was supplied";

		if (!empty($_POST["password1"]))
			$password1 = trim($_POST["password1"]);
		else
			$error[] = "No password was supplied";

		if (!empty($_POST["password2"]))
			$password2 = trim($_POST["password2"]);
		else
			$error[] = "No password was supplied";

		if (!empty($_POST["email"]))
			$email = trim($_POST["email"]);
		else
			$error[] = "No email address was supplied";

		//If we still don't have an error
		if (empty($error))
		{
			//Check that the two passwords are the same
			if ($password1 != $password2)
				$error[] = "Passwords do not match";

			//Check that the email address is valid
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				$error[] = "Email address is not valid";

			//Validate usrename and password, checking for allowed characters
			if (!CMSEValidator::ValidateUsername($username))
				$error[] = "Username supplied is invalid";

			if (!CMSEValidator::ValidatePassword($password1) || !CMSEValidator::ValidatePassword($password2))
				$error[] = "Password supplied is invalid";

			//If all this has succeeded, then registration is valid. Go!
			if (empty($error))
			{
				global $g_cmseUser;

				//Wait. First check if a user with that username exists or not.
				if ($g_cmseUser->CheckIfUserExists($username))
					$error[] = "Username already exists";
				else
				{
					//Else, register the user
					$check = $g_cmseUser->Register($username, $password1, $email);

					if(!$check)
						$error[] = "Could not register you. Please try again later.";
				}
			}

		}
	}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="admin/theme/favicon.ico" type="image/x-icon" />
	<title>CMSE Admin Panel - Register</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, admin" /> 
	<meta name="Description" content="CMSE Admin Panel" /> 

	<link rel="stylesheet" type="text/css" href="admin/theme/style.login.css" />
</head>
<body>
	<div id="container">
		<h2>Register</h2>
<?PHP
	//If we have errors, echo them
	if (isset($_POST["registerSubmit"]) && !empty($error))
	{
		foreach ($error as $value)
			echo '<p class="error">' . $value . '</p>';

		echo '<br />';
	}

	//First check if registration is disabled
	if(!CMSE_REGENABLED)
	{
		echo '<p class="error">Sorry, registration is currently disabled.</p>';
	}
	//If no one had hit the submit button, or there are errors, then echo out the form/
	else if (!isset($_POST["registerSubmit"]) || (isset($_POST["registerSubmit"]) && !empty($error)))
	{
?>
		<form name="registerform" id="registerform" action="register.php" method="post">
			<label for="username">Username <br /><input type="text" name="username" id="username" value="<?PHP echo (!empty($_POST["username"]) ? $_POST["username"] : "" ); ?>" size="20" /></label>
			<label for="password1">Password <br /><input type="password" name="password1" id="password1" value="<?PHP echo (!empty($_POST["password1"]) ? $_POST["password1"] : "" ); ?>" size="20" /></label>
			<label for="password2">Repeat Password <br /><input type="password" name="password2" id="password2" value="<?PHP echo (!empty($_POST["password2"]) ? $_POST["password2"] : "" ); ?>" size="20" /></label>
			<label for="email">Email Address <br /><input type="text" name="email" id="email" value="<?PHP echo (!empty($_POST["email"]) ? $_POST["email"] : "" ); ?>" size="20" /></label>
			<input type="submit" name="registerSubmit" id="registerSubmit" value="Register" />
		</form>
<?PHP
	}
	else
	{
?>
		<p class="success">Registration was successful!</p>
<?PHP
	}
?>
	</div>
</body>
</html>